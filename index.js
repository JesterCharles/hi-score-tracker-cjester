const express = require("express");

const app = express();

const scores = [];

app.get("/scores", (req, res) => {
	res.status(500);
	res.send("Not Implemented yet");
});

app.post("/scores", (req, res) => {
	// {intials:"", points:100}
	const score = req.body;
	scores.push(score);
	res.send("New score has been successfully added");
});

app.listen(3000, () => console.log("application started"));
